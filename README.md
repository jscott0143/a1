How to Run A1 - Bit Bucket Testing app.

Step 1: Download project file from bit bucket by cloning repo.

Step 2: locate the A1 - BitBucket Tester App.SLN file on computer and run via Visual Studio.

Step 3: Once Visual Studio opens project file, click the Play/Start A1 - BitBucket Tester App located at the top middle section.

Step 4: Enter First Name when promped in Console. Press Enter to see results.

Step 5: Close Applicaiton when no longer interested. 